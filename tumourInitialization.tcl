# Amira Script

remove -all

source $SCRIPTDIR/helpers/matrixMult.tcl
proc pause {{message "Hit Enter to continue ==> "}} {
    puts -nonewline $message
    flush stdout
    gets stdin
}

set fp [open $SCRIPTDIR/tumourMisaligned.txt r]
set file_data [read $fp]
close $fp
set allNames [split $file_data \n]

file mkdir $SCRIPTDIR/tumourOutput
set prevFileLeading 000_0

foreach line $allNames {
    if {[string range $line 0 4] ne $prevFileLeading} {
        remove -all
        set patient [string range $line 0 2]
        set timeSplit [split $line _m]
        set timePt [lindex $timeSplit 1]
        [load -nifti {*}[glob [format $SCRIPTDIR/ConvertedDicoms/%s/%s*/*/*.nii $patient $timePt]]] setLabel Corrected
        [load -nifti {*}[glob [format $SCRIPTDIR/ConvertedDicoms/%s/initial/*/*.nii $patient]]] setLabel Initial

        set invTranslate [Initial getInverseTransform]

        set registration [create HxAffineRegistration]
        $registration model connect Initial
        $registration reference connect Corrected
        $registration action setValue 0
        $registration fire
        $registration action setValue 2
        $registration fire
        
        set regMatrix [Initial getTransform]
        set prevFileLeading [string range $line 0 4]
    }
    [load -nifti $SCRIPTDIR/ConvertedSegmentations/$line] setLabel VB

    set levelSplit [split $line _] 
    set splitIdx [llength $levelSplit]
    set splitIdx [expr {$splitIdx - 2}]
    set level [lindex $levelSplit $splitIdx]
    set cropName {*}[glob $SCRIPTDIR/ConvertedSegmentations/$patient\_$timePt*$level*Scan*.nii]
    [load -nifti $cropName] setLabel Cropped

    set LPStoRAS {-10 0 0 0 0 -10 0 0 0 0 10 0 0 0 0 1}
    VB setTransform {*}[matrixMult $LPStoRAS [VB getTransform]]
    VB setTransform {*}[matrixMult $invTranslate [VB getTransform]]
    VB setTransform {*}[matrixMult $regMatrix [VB getTransform]]

    Cropped setTransform {*}[matrixMult $LPStoRAS [Cropped getTransform]]
    Cropped setTransform {*}[matrixMult $invTranslate [Cropped getTransform]]
    Cropped setTransform {*}[matrixMult $regMatrix [Cropped getTransform]]

    VB save Nifti $SCRIPTDIR/tumourOutput/$line

    set cropFileName $patient\_$timePt\_Months_$level\_ScanTumour$level.nii
    Cropped save Nifti $SCRIPTDIR/tumourOutput/$cropFileName

    remove $line
    remove $cropFileName
}