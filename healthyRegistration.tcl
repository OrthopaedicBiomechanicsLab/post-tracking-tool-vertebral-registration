# Amira Script

set fp [open $SCRIPTDIR/healthyMisaligned.txt r]
set misaligned_data [read $fp]
close $fp

set allMisaligned [split $misaligned_data \n]

set idx 0

file mkdir $SCRIPTDIR/healthyOutput

foreach misalignedName $allMisaligned {

    [load -nifti $SCRIPTDIR/ConvertedSegmentations/$misalignedName] setLabel Misaligned
    set alignedName [string map {BCo BNoCo} $misalignedName]
    [load -nifti $SCRIPTDIR/ConvertedSegmentations/$alignedName] setLabel Aligned
    
    set registration [create HxAffineRegistration]
    $registration model connect Misaligned
    $registration reference connect Aligned
    $registration action setValue 0
    $registration fire

    Misaligned save Nifti $SCRIPTDIR/healthyOutput/$misalignedName

    remove -all
}