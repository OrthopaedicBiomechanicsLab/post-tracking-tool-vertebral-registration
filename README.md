# Post-Tracking-Tool Registration Tool

This repository contains a number of Amira and 3D Slicer tools that will correct the alignment of scans and segmentations produced by the Amira Tracking Tool for rat stereology. 

It is sometimes necessary to correct the output of the tracking tool as segmentations are aligned to the frame of reference of the first time point, causing subsequent time points to fall out of alignment with respect to the scans they are trying to segment.

SlicerRegistrationTool contains a scripted loadable module for Slicer, whereas healthRegistration.tcl and tumourInitialization.tcl are simply scripts that can be opened in Amira to execute them.

## Getting Started

Clone this repository into a local directory by navigating to that directory in the command line and typing:

```
git clone https://teeohem@bitbucket.org/OrthopaedicBiomechanicsLab/post-tracking-tool-vertebral-registration.git
```

OR

Click the left drop-down menu, then Downloads > Download repository. Extract the resulting .zip file into a directory of your choice.

### Prerequisites

You will need: 

3D Slicer, tested with version 4.8 and above

Amira 5.X

A SpineMets data set that is in Nifti format, and structured such that the subfolder path leading to each scan is:
```
MetsScans\<patient number padded to 3 digits>\<time point in months or "initial">\<range of vertebrae>\<scan name>.nii
```

A list of all subscan and segmentation files in Nifti format, and named:
```
<patient number padded to 3 digits>_<time points in months>_<vertebra level>_<subscan or segmentation name>.nii
```
contained within a folder called:
```
ConvertedSegmentations
```

Additionally, 2 text files need to be created to indicate the names of each of the files to be corrected. Because the tracking tool produces different types of errors for tumour and healthy vertebrae, the names of all *healthy* misaligned scans are to be listed as rows in healthyMisaligned.txt, while *tumour* misaligned scans will be in tumourMisaligned.txt.

Correctly formatted MetsScans and ConvertedSegmentations folders, as well as corresponding text files, are To work properly, the folders and text files must be contained in the same directory as the .tcl files and SlicerRegistrationTool.

### Installing

The .tcl files are ready to use as is, and simply require the aforementioned prerequisites.

To install the Slicer module, open 3D Slicer, and click Edit > Application Settings > Modules > Add. Navigate the directory containing SlicerRegistrationTool and select it. Restart Slicer as prompted.

## Usage

### Registration of Healthy Segmentations

Once all of the prerequisites are in place, open Amira 5, and drag the healthyRegistration.tcl file into Amira. The script should execute automatically.

### Registration of Tumour Segmentations

As with before, drag tumourInitialization.tcl into Amira. This will give 3D Slicer a rough alignment to begin rigid registration from. Without this step, it won't converge.

Next, open 3D Slicer, and click the Modules drop-down menu (it should read "Welcome to Slicer" initially) and select Registration > RegisterVB. Hit "Apply"

## Bugs

For text files listing more than 50 tumour files, 3D Slicer can unexpectedly crash during execution. It is suspected that Qt is causing the crash. Until a fix is found, limit list lengths to 50 and break large data sets into smaller subsets if necessary.

## Authors

* **Tom Wei**

## Acknowledgments

Thanks to:

* Michael Hardisty for all his help and guidance with 3D Slicer and Amira scripting

* Geoffrey Klein for converting a large set of DICOMs and Amira binaries into the much-nicer Nifti format for this project