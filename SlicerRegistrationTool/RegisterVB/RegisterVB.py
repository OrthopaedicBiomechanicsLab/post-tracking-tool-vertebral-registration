import os, re
import unittest
import vtk, qt, ctk, slicer
from slicer.ScriptedLoadableModule import *
import logging
import fnmatch

#
# RegisterVB
#

class RegisterVB(ScriptedLoadableModule):
  """Uses ScriptedLoadableModule base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def __init__(self, parent):
    ScriptedLoadableModule.__init__(self, parent)
    self.parent.title = "RegisterVB" # TODO make this more human readable by adding spaces
    self.parent.categories = ["Examples"]
    self.parent.dependencies = []
    self.parent.contributors = ["John Doe (AnyWare Corp.)"] # replace with "Firstname Lastname (Organization)"
    self.parent.helpText = """
This is an example of scripted loadable module bundled in an extension.
It performs a simple thresholding on the input volume and optionally captures a screenshot.
"""
    self.parent.helpText += self.getDefaultModuleDocumentationLink()
    self.parent.acknowledgementText = """
This file was originally developed by Jean-Christophe Fillion-Robin, Kitware Inc.
and Steve Pieper, Isomics, Inc. and was partially funded by NIH grant 3P41RR013218-12S1.
""" # replace with organization, grant and thanks.

#
# RegisterVBWidget
#

class RegisterVBWidget(ScriptedLoadableModuleWidget):
  """Uses ScriptedLoadableModuleWidget base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setup(self):
    ScriptedLoadableModuleWidget.setup(self)

    # Instantiate and connect widgets ...

    #
    # Parameters Area
    #
    parametersCollapsibleButton = ctk.ctkCollapsibleButton()
    parametersCollapsibleButton.text = "Parameters"
    self.layout.addWidget(parametersCollapsibleButton)

    # Layout within the dummy collapsible button
    parametersFormLayout = qt.QFormLayout(parametersCollapsibleButton)

    #
    # Apply Button
    #
    self.applyButton = qt.QPushButton("Apply")
    self.applyButton.toolTip = "Run the algorithm."
    parametersFormLayout.addRow(self.applyButton)

    # connections
    self.applyButton.connect('clicked(bool)', self.onApplyButton)

    # Add vertical spacer
    self.layout.addStretch(1)

  def cleanup(self):
    pass

  def onApplyButton(self):
    logic = RegisterVBLogic()
    logic.run()

#
# RegisterVBLogic
#

class RegisterVBLogic(ScriptedLoadableModuleLogic):
  """This class should implement all the actual
  computation done by your module.  The interface
  should be such that other python code can import
  this class and make use of the functionality without
  requiring an instance of the Widget.
  Uses ScriptedLoadableModuleLogic base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """
  def getNodes(self):
    nodes = slicer.mrmlScene.GetNodes()
    return [nodes.GetItemAsObject(i).GetID() for i in xrange(0,nodes.GetNumberOfItems())]

  def registerTo(self, scriptDir, scan, prevFileLeading):

    if scan[0][0:5] != prevFileLeading:
      try:     
        oldNode = slicer.util.getNode("Ref*")
      except:
        oldNode = None

      if oldNode is not None: 
        slicer.mrmlScene.RemoveNode(oldNode.GetDisplayNode())
        slicer.mrmlScene.RemoveNode(oldNode.GetStorageNode())
        slicer.mrmlScene.RemoveNode(oldNode)

      fPath = os.path.dirname(os.path.dirname(scriptDir)) + "/convertedDicoms/" + scan[0][0:3] + "/"
      for subFold in os.listdir(fPath):
        if scan[0][4] in subFold:
          fPath += subFold
          break

      fPath += "/" + os.listdir(fPath)[0]
      fPath += "/" + os.listdir(fPath)[0]
      success, referenceNode = slicer.util.loadVolume(fPath, returnNode = True)
      referenceNode.SetName('Ref')
      prevFileLeading = scan[0][0:5]

    cropName = scan[0][0:3] + "_" + scan[2] + "_Months_" + scan[1] + "_Scan_Healthy.nii"      
    success, volumeNode = slicer.util.loadVolume(os.path.dirname(os.path.dirname(scriptDir)) + "/ConvertedSegmentations/" + scan[0], returnNode = True)
    referenceNode = slicer.util.getNode("Ref*")
    success, cropNode = slicer.util.loadVolume(os.path.dirname(os.path.dirname(scriptDir)) + "/ConvertedSegmentations/" + cropName, returnNode = True)

    brainsFit = slicer.modules.brainsfit
    fitParameters = {}
    fitParameters["fixedVolume"] = referenceNode.GetID()
    fitParameters["movingVolume"] = cropNode.GetID()
    fitParameters["samplingPercentage"] = 0.02
    fitParameters["splineGridSize"] = "14, 10, 12"
    fitParameters["useRigid"] = True

    MSETransform = slicer.vtkMRMLLinearTransformNode()
    MSETransform.SetName("MSETransform")
    slicer.mrmlScene.AddNode( MSETransform )
    fitParameters["linearTransform"] = MSETransform.GetID()
    fitParameters["costMetric"] = "MSE"

    cliRigidRegNode = None
    cliRigidRegNode = slicer.cli.run(brainsFit, None, fitParameters)
    waitCount = 0
    while cliRigidRegNode.GetStatusString() != 'Completed':
      self.delayDisplay( "Performing Rigid Registration... %d" % waitCount, msec = 1000 )
      #print cliFiducialRegNode.GetStatusString()
      waitCount += 1
      if waitCount > 1199:
        self.delayDisplay("Registration Timed Out!")
        break
    self.delayDisplay("Rigid Registration finished")

    slicer.mrmlScene.RemoveNode(cliRigidRegNode)

    cropNode.SetAndObserveTransformNodeID(MSETransform.GetID())
    volumeNode.SetAndObserveTransformNodeID(MSETransform.GetID())      
    cropNode.HardenTransform()
    volumeNode.HardenTransform()

    brainsResample = slicer.modules.brainsresample
    parameters = {}
    parameters["referenceVolume"] = cropNode.GetID()
    parameters["interpolationMode"] = 'WindowedSinc'
    
    parameters["inputVolume"] = referenceNode.GetID()
    windowedReference = slicer.vtkMRMLScalarVolumeNode()
    windowedReference.SetName(referenceNode.GetName()+"_Windowed")
    slicer.mrmlScene.AddNode( windowedReference )
    parameters["outputVolume"] = windowedReference.GetID()

    cliBrainsResampleNode = None
    cliBrainsResampleNode = slicer.cli.run(brainsResample, None, parameters)
    waitCount = 0
    while cliBrainsResampleNode.GetStatusString() != 'Completed':
      self.delayDisplay( "Resampling... %d" % waitCount, msec = 1000)
      waitCount += 1
    self.delayDisplay("Resampling finished")

    slicer.mrmlScene.RemoveNode(cliBrainsResampleNode)

    finalFit = slicer.modules.brainsfit
    fitParameters = {}
    fitParameters["fixedVolume"] = windowedReference.GetID()
    fitParameters["movingVolume"] = cropNode.GetID()
    fitParameters["samplingPercentage"] = 0.02
    fitParameters["splineGridSize"] = "14, 10, 12"
    fitParameters["useRigid"] = True

    MMITransform = slicer.vtkMRMLLinearTransformNode()
    MMITransform.SetName("MMITransform")
    slicer.mrmlScene.AddNode( MMITransform )
    fitParameters["linearTransform"] = MMITransform.GetID()
    fitParameters["costMetric"] = "MMI"

    #pause = raw_input("program paused; press <ENTER>")

    cliFinalRegNode = None
    cliFinalRegNode = slicer.cli.run(finalFit, None, fitParameters)
    waitCount = 0
    while cliFinalRegNode.GetStatusString() != 'Completed':
      self.delayDisplay( "Performing Rigid Registration... %d" % waitCount, msec = 1000 )
      #print cliFiducialRegNode.GetStatusString()
      waitCount += 1
      if waitCount > 1199:
        self.delayDisplay("Registration Timed Out!")
        break
    self.delayDisplay("Rigid Registration finished")

    slicer.mrmlScene.RemoveNode(cliFinalRegNode)

    cropNode.SetAndObserveTransformNodeID(MMITransform.GetID())
    volumeNode.SetAndObserveTransformNodeID(MMITransform.GetID())      
    cropNode.HardenTransform()
    volumeNode.HardenTransform()

    inverseScaleLPS = vtk.vtkMatrix4x4()
    inverseScaleLPS.DeepCopy([-0.1, 0, 0, 0, 0, -0.1, 0, 0, 0, 0, 0.1, 0, 0, 0, 0, 1])
    
    cropNode.ApplyTransformMatrix(inverseScaleLPS)
    volumeNode.ApplyTransformMatrix(inverseScaleLPS)

    slicer.util.saveNode(cropNode, os.path.dirname(os.path.dirname(scriptDir)) + "/slicerOutput/" + cropName)
    slicer.util.saveNode(volumeNode, os.path.dirname(os.path.dirname(scriptDir)) + "/slicerOutput/" + scan[0])

    slicer.mrmlScene.RemoveNode(volumeNode.GetDisplayNode())
    slicer.mrmlScene.RemoveNode(volumeNode.GetStorageNode())
    slicer.mrmlScene.RemoveNode(volumeNode)
    slicer.mrmlScene.RemoveNode(cropNode.GetDisplayNode())
    slicer.mrmlScene.RemoveNode(cropNode.GetStorageNode())
    slicer.mrmlScene.RemoveNode(cropNode)
    slicer.mrmlScene.RemoveNode(windowedReference.GetDisplayNode())
    slicer.mrmlScene.RemoveNode(windowedReference.GetStorageNode())
    slicer.mrmlScene.RemoveNode(windowedReference)
    slicer.mrmlScene.RemoveNode(MSETransform.GetDisplayNode())
    slicer.mrmlScene.RemoveNode(MSETransform.GetStorageNode())
    slicer.mrmlScene.RemoveNode(MSETransform)
    slicer.mrmlScene.RemoveNode(MMITransform.GetDisplayNode())
    slicer.mrmlScene.RemoveNode(MMITransform.GetStorageNode())
    slicer.mrmlScene.RemoveNode(MMITransform)

    return prevFileLeading

  def run(self):
    """
    Run the actual algorithm
    """
    scriptDir = os.path.dirname(slicer.modules.registervb.path)
    misalignedList= []

    nodes0 = self.getNodes()

    if not os.path.exists(os.path.dirname(os.path.dirname(scriptDir)) + "/slicerOutput"):
      os.makedirs(os.path.dirname(os.path.dirname(scriptDir)) + "/slicerOutput")

    with open(os.path.dirname(os.path.dirname(scriptDir)) + "/healthyMisaligned019.txt") as file:
      for line in file:
        line = line.strip()
        noUnderscores = line.split("_")
        timeFinder = re.split('m|_', line)
        level = noUnderscores[-2]
        time = timeFinder[1]
        misalignedList.append((line, level, time))

    prevFileLeading = "000_0"

    for scan in misalignedList:

      prevFileLeading = self.registerTo(scriptDir, scan, prevFileLeading)
      # nodes3 = self.getNodes()

      # for x in nodes3:
      #   if x not in nodes0:
      #     print x

      #pause = raw_input("program paused; press <ENTER>")

    return True


class RegisterVBTest(ScriptedLoadableModuleTest):
  """
  This is the test case for your scripted module.
  Uses ScriptedLoadableModuleTest base class, available at:
  https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
  """

  def setUp(self):
    """ Do whatever is needed to reset the state - typically a scene clear will be enough.
    """
    slicer.mrmlScene.Clear(0)

  def runTest(self):
    """Run as few or as many tests as needed here.
    """
    self.setUp()
    self.test_RegisterVB1()

  def test_RegisterVB1(self):
    """ Ideally you should have several levels of tests.  At the lowest level
    tests should exercise the functionality of the logic with different inputs
    (both valid and invalid).  At higher levels your tests should emulate the
    way the user would interact with your code and confirm that it still works
    the way you intended.
    One of the most important features of the tests is that it should alert other
    developers when their changes will have an impact on the behavior of your
    module.  For example, if a developer removes a feature that you depend on,
    your test should break so they know that the feature is needed.
    """

    self.delayDisplay("Starting the test")
    #
    # first, get some data
    #
    import urllib
    downloads = (
        ('http://slicer.kitware.com/midas3/download?items=5767', 'FA.nrrd', slicer.util.loadVolume),
        )

    for url,name,loader in downloads:
      filePath = slicer.app.temporaryPath + '/' + name
      if not os.path.exists(filePath) or os.stat(filePath).st_size == 0:
        logging.info('Requesting download %s from %s...\n' % (name, url))
        urllib.urlretrieve(url, filePath)
      if loader:
        logging.info('Loading %s...' % (name,))
        loader(filePath)
    self.delayDisplay('Finished with download and loading')

    cropNode = slicer.util.getNode(pattern="FA")
    logic = RegisterVBLogic()
    self.delayDisplay('Test passed!')
